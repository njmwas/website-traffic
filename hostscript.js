((d, w)=>{
    //Change this to the location of your php file that will receive the post
    // Note: there is no leading '/'
    const receivingUrl='analytics/registerTraffic.php', 

    s = d.currentScript.src, h=s.split('/')[2], ls=localStorage, searchParams=(search=w.location.search).substr(1, search.length).split('&').reduce((a, b)=>{ a[(p=b.split('='))[0]]=p[1]; return a; }, {});
    let params = { timein:(new Date()).getTime(), duration:0, referrer:d.referrer, current_url:w.location.href, isActive:true, activetime:0}, lsparams=JSON.parse(ls.getItem('opskillanalytics')||'{}');
    'opclick' in searchParams ? (params['afreferrer']=searchParams['opclick']) : (Object.keys(lsparams).length && 'afreferrer' in lsparams && Object.assign(params, {afreferrer:lsparams['afreferrer']}));
    !Object.keys(lsparams).length && ls.setItem('opskillanalytics', JSON.stringify(params));
    d.addEventListener('visibilitychange', evt=>{ params.isActive && (params.activetime+=evt.timeStamp); params.isActive = !params.isActive; });
    let goForIt = (state)=>{
        console.log(d.referrer);
        if(state=='load' && Object.keys(lsparams).length && params.current_url===lsparams.current_url) Object.assign(lsparams, params);
        else ls.setItem('opskillanalytics', JSON.stringify(params));
        params.duration+=((new Date()).getTime()-params.timein); let fd = new FormData();
        Object.keys(params).map(p=>fd.append(p, params[p])); 
        navigator.sendBeacon(['','', h, receivingUrl].join('/'), fd);
    }
    w.onload = ()=>window.setTimeout(()=>goForIt('load'), 200); 
    w.onunload=()=>goForIt('unload');
})(document, window);