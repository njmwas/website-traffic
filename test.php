<?
echo 'curling';

// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'http://google.com'
));

// Send the request & save response to $resp
$resp = curl_exec($curl);
if(!$resp){
    die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
}
// Close request to clear up some resources
curl_close($curl);

if(!$resp) echo $resp;