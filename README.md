###### Hello there

Host 'hostscript.js' in your js directory;

then put the following code at the bottom of the body tag on every page of the website you are tracking traffic

```
((d, id)=>{
    if(!d.getElementById(id)){  
        const s = d.createElement('script');
        s.id=id;
        //Change the url to the javascript that posts the current url and the site reference
        s.src = `http://localhost:86/analytics/hostscript.js?${Math.random()}`;
        d.head.append(s);
    }
})(document, 'opanAlyticsRoot');
```

as illustrated on 'index.html' file

Note: 
1. replace the value of s.src with the url where 'hostscript.js' is;
2. on 'hostscript.js' change the receivingUrl value to point to the php page that receives the post

###### Parameters Posted
1. timein - the date in milliseconds that the page was accessed
2. duration - how many milliseconds the user spent on the page
3. referrer - the url of the page that referred to this page (empty string if it was accessed directly)
4. current_url - the current page
5. activetime - the time that the user was actively on this page (did not navigate away)
6. afreferrer - the tracking cookie. It's sent only when it is set otherwise it is not sent

Regards